import createElement from './vdom/createElement';
import render from './vdom/render';
import mount from './vdom/mount';
import diff from './vdom/diff';

// 虚拟DOM树
const createVApp = (count = 0)=> createElement('div', { 
    attrs: {
        id: 'app'
    },
    children: [
        createElement('img', { 
            attrs: {
                src: 'https://feat-assets.oss-cn-beijing.aliyuncs.com/aet/u%3D2147391096%2C1086201754%26fm%3D26%26gp%3D0.jpg'
            },
            children: [],
        }),
        createElement('p', { 
            attrs: {
                class: 'context'
            },
            children:["hello world, this is a p tag"]
        }),
        String(count),
        createElement('div', { 
            attrs: {
                value: ''
            },
            children:[]
        })
    ]
})

let count = 0;

//虚拟DOM树
let vApp = createVApp(count);
//根据虚拟DOM树生成真实的DOM树
let $app = render(vApp);
//将真实的DOM挂载在ID为'app'的节点上
let $rootEle = mount($app, document.getElementById('app'));

//模拟属性改变时候的 更新 机制  ⭐️⭐️⭐️⭐️⭐️  核心逻辑是 diff 和 parch
setInterval(()=>{

    count ++;
    // 重新创建虚拟DOM
    const vNewApp = createVApp(count);
    // 计算差异布丁
    const patch = diff(vApp, vNewApp);
    // 重新渲染DOM
    $rootEle = patch($rootEle);
    // 更新DOM之后, 更新最新的虚拟DOM，方便下次计算
    vApp = vNewApp;

},1000)

