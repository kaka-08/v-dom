//挂载 将目标DOM 替换为 计算之后的DOM
export default ($node, $target) => {
    $target.replaceWith($node);
    return $node;
}