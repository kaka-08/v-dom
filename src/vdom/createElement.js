/**
 * this is createElement func, aim to create a virtual DOM
 * 虚拟DOM? 一个简单的js对象 （ plain ）
 */
export default (tagName, { attrs = {},children = [] }) => {
    return {
        tagName,
        attrs,
        children 
    }
}